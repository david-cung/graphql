const { Query } = require("./query");

const resolvers = {
  Query: Query,
};

module.exports = { resolvers };
