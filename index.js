require("dotenv").config();
const { ApolloServer } = require("apollo-server");
const { typeDefs } = require("./schema/schema");
const { resolvers } = require("./resolvers/resolver");

const server = new ApolloServer({ typeDefs, resolvers });

server.listen(process.env.PORT || 3000, () => {
  console.log("server is starting");
});
